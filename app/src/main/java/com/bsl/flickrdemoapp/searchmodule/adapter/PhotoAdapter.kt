package com.bsl.flickrdemoapp.searchmodule.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bsl.flickrdemoapp.databinding.PhotoHolderLayoutBinding
import com.bsl.flickrdemoapp.searchmodule.fragment.PhotoGridFragmentDirections
import com.bsl.flickrdemoapp.searchmodule.model.PhotoMetaDataDetails

class PhotoAdapter :
    ListAdapter<PhotoMetaDataDetails, PhotoAdapter.ViewHolder>(DiffCallback()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            PhotoHolderLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it, createOnClickListener(it)) }
    }

    class ViewHolder(private val binding: PhotoHolderLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(photoMetaDataDetails: PhotoMetaDataDetails, listener: View.OnClickListener) {
            with(itemView) {
                binding.apply {
                    photoMetaData = photoMetaDataDetails
                    onItemClicked = listener
                }
            }
        }
    }

    private fun createOnClickListener(photoMetaDataDetails: PhotoMetaDataDetails): View.OnClickListener {
        return View.OnClickListener {
            it.findNavController().navigate(
                PhotoGridFragmentDirections.actionPhotoGridFragmentToPhotoFullViewFragment(
                    photoMetaDataDetails
                )
            )
        }
    }
}

private class DiffCallback : DiffUtil.ItemCallback<PhotoMetaDataDetails>() {

    override fun areItemsTheSame(
        oldItem: PhotoMetaDataDetails,
        newItem: PhotoMetaDataDetails
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: PhotoMetaDataDetails,
        newItem: PhotoMetaDataDetails
    ): Boolean {
        return oldItem == newItem
    }
}