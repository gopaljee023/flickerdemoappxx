package com.android.flickphoto.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.bsl.flickrdemoapp.searchmodule.model.PhotoMetaDataDetails

@Dao
interface FlickrPhotoDao{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPhotos(vararg photos: PhotoMetaDataDetails)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPhoto(photo: PhotoMetaDataDetails)

    @Update
    fun update(photo: PhotoMetaDataDetails)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertListPhotos(photos:List<PhotoMetaDataDetails>)


    @Query("SELECT * FROM photos WHERE searchvalue LIKE '%' || :query || '%'")
    fun searchPhotos(query:String): LiveData<List<PhotoMetaDataDetails>>

    @Query("SELECT * FROM photos")
    fun getPhotos(): LiveData<List<PhotoMetaDataDetails>>

    @Query("SELECT COUNT(*) from photos WHERE searchvalue IN (:value)")
    fun getCount(value:String): Int

}