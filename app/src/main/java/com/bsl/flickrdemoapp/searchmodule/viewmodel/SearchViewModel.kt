package com.gmbmerchant.promo.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.flickphoto.db.FlickrPhotoDatabase
import com.bsl.flickrdemoapp.searchmodule.api.NETWORK_PAGE_SIZE
import com.bsl.flickrdemoapp.searchmodule.api.SchedulersWrapper
import com.bsl.flickrdemoapp.searchmodule.model.MainModel
import com.bsl.flickrdemoapp.searchmodule.model.PhotosResponse
import com.bsl.flickrdemoapp.searchmodule.view.ISearchView
import io.reactivex.observers.DisposableSingleObserver
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SearchViewModel(var schedulersWrapper: SchedulersWrapper, val searchview: ISearchView) : ViewModel() {
    val resultListObservable = MutableLiveData<PhotosResponse>()
    val resultListErrorObservable = MutableLiveData<String>()
    fun getResultListObservable(): LiveData<PhotosResponse> = resultListObservable
    fun getResultListErrorObservable(): LiveData<String> = resultListErrorObservable
    lateinit var mainModel: MainModel

    fun SearchService(query: String, page: Int, db: FlickrPhotoDatabase) {
        searchview.showProgress()
        CoroutineScope(Dispatchers.IO).launch {
//            var count:Int = db.flickrPhotoDao.getCount("trees")/ NETWORK_PAGE_SIZE+1
            mainModel.SearchAttribute(query,page).subscribeOn(schedulersWrapper.io())
                .observeOn(schedulersWrapper.main())
                .subscribeWith(object : DisposableSingleObserver<PhotosResponse>() {
                    override fun onSuccess(response: PhotosResponse) {
                        searchview.hideProgress()
                        val results = response.photos?.photoList ?: emptyList()
                        results.forEachIndexed { index, photoMetaDataDetails ->  results.get(index).searchvalue = query}
                        if (results.size>0) {
                            CoroutineScope(Dispatchers.IO).launch{
                                db.flickrPhotoDao.insertListPhotos(results)
                            }
                            resultListObservable.postValue(response)
                        }else{
                            resultListErrorObservable.postValue("t.photos.status")
                        }
                    }

                    override fun onError(e: Throwable) {
                        searchview.hideProgress()
                        resultListErrorObservable.postValue(e.message)
                    }
                })

            return@launch withContext(Dispatchers.IO) {
//                searchview.hideProgress()
            }
        }
    }

}