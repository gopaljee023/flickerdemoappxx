package com.gmbmerchant.retrofit


import com.bsl.flickrdemoapp.searchmodule.model.PhotosResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*

interface ApiInterface {

    @GET(".")
    fun getPhotosMetaData(
        @Query("page") perpage: Int,
        @Query("per_page") page: Int,
        @Query("method") method: String,
        @Query("api_key") apiKey: String,
        @Query("user_id", encoded = true) userId: String,
        @Query("format") format: String,
        @Query("nojsoncallback") noJsonCallback: Int,
        @Query("text") text: String
    ): Single<PhotosResponse>

}