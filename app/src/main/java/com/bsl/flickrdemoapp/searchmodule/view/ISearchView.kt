package com.bsl.flickrdemoapp.searchmodule.view

interface ISearchView {
    fun showProgress()
    fun hideProgress()
}
