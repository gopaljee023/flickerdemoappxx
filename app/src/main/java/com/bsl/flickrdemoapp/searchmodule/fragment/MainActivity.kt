package com.bsl.flickrdemoapp.searchmodule.fragment

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bsl.flickrdemoapp.R
import com.bsl.flickrdemoapp.databinding.ActivityMainBinding
import dagger.android.support.DaggerAppCompatActivity

public class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
    }
}
