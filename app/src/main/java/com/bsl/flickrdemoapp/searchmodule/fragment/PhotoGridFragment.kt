package com.bsl.flickrdemoapp.searchmodule.fragment


import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.flickphoto.db.FlickrPhotoDatabase
import com.bsl.flickrdemoapp.R
import com.bsl.flickrdemoapp.databinding.FragmentPhotoGridBinding
import com.bsl.flickrdemoapp.searchmodule.utils.GridSpacingItemDecoration
import com.bsl.flickrdemoapp.searchmodule.utils.PaginationListener
import com.bsl.flickrdemoapp.searchmodule.adapter.PhotoAdapter
import com.bsl.flickrdemoapp.searchmodule.api.SchedulersWrapper
import com.bsl.flickrdemoapp.searchmodule.model.MainModel
import com.bsl.flickrdemoapp.searchmodule.model.PhotoMetaDataDetails
import com.bsl.flickrdemoapp.searchmodule.view.ISearchView
import com.gmbmerchant.promo.model.SearchViewModel
import com.gmbmerchant.promo.model.SearchViewModelFactory
import kotlinx.android.synthetic.main.fragment_photo_grid.*

class PhotoGridFragment : Fragment(), ISearchView {

    private lateinit var binding: FragmentPhotoGridBinding
    private lateinit var photoVM: SearchViewModel
    private lateinit var photoAdapter: PhotoAdapter
    private var isProgress: Boolean = false
    private lateinit var layoutManager: GridLayoutManager
    private var dataList = mutableListOf<PhotoMetaDataDetails>()
    private var pageNumber: Int = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPhotoGridBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        photoVM = ViewModelProvider(this, SearchViewModelFactory(SchedulersWrapper(), this))
            .get(SearchViewModel::class.java)
        photoVM.mainModel = MainModel()
        val db: FlickrPhotoDatabase = FlickrPhotoDatabase.getInstance(context = requireActivity())

        layoutManager = GridLayoutManager(activity, 2)
        photoAdapter = PhotoAdapter()
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.grid_layout_margin)
        rvPhotos.layoutManager = layoutManager
        rvPhotos.addItemDecoration(
            GridSpacingItemDecoration(
                2,
                spacingInPixels,
                true,
                0
            )
        )
        binding.rvPhotos.adapter = photoAdapter

        searchText.isFocusableInTouchMode = false
        val searchManager = activity?.getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchText.setSearchableInfo(
            searchManager
                .getSearchableInfo(activity?.getComponentName())
        )
        searchText.maxWidth = Int.MAX_VALUE

        binding.clickOnRetry = View.OnClickListener {
//            fetchData()
        }

        searchText.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                pageNumber = 1
                dataList.clear()
                photoAdapter.submitList(dataList)
                photoVM.SearchService(query, pageNumber, db)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                return false
            }
        })

        photoVM.getResultListObservable().observe(viewLifecycleOwner, Observer {
            dataList.addAll(it.photos.photoList)
            photoAdapter.submitList(dataList)
            photoAdapter.notifyDataSetChanged()
            pageNumber = it.photos.page + 1
            db.flickrPhotoDao.getPhotos().observe(viewLifecycleOwner, Observer { d ->
                d.size
                isProgress = false
            })
        })

        photoVM.getResultListErrorObservable().observe(viewLifecycleOwner, Observer {
            db.flickrPhotoDao.searchPhotos(searchText.query.toString())
                .observe(viewLifecycleOwner, Observer { d ->
                    dataList.clear()
                    dataList.addAll(d)
                    photoAdapter.submitList(dataList)
                })
        })

        rvPhotos.addOnScrollListener(object : PaginationListener(layoutManager) {
            override fun loadMoreItems() {
                if (!isProgress) {
                    photoVM.SearchService(searchText.query.toString(), pageNumber, db)
                    isProgress = true
                }
            }

            override val isLastPage: Boolean
                get() = false
            override val isLoading: Boolean
                get() = false

        });

        photoAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                layoutManager.scrollToPositionWithOffset(positionStart, 0)
            }
        })

    }

    override fun showProgress() {
        binding.llProgress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        binding.llProgress.visibility = View.GONE
    }


}


