package com.bsl.flickrdemoapp.searchmodule.utils;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

@GlideModule
public class CustomeGlideModule extends AppGlideModule {}