package com.bsl.flickrdemoapp.searchmodule.utils

import android.app.Activity
import android.app.Application
import dagger.android.DispatchingAndroidInjector
import javax.inject.Inject

class FlickrApplication : Application() {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Activity>


    override fun onCreate() {
        super.onCreate()
        AppInjector.init(this)
    }
}