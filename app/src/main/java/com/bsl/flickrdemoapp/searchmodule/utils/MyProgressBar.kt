package com.bsl.flickrdemoapp.searchmodule.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.ColorDrawable
import com.bsl.flickrdemoapp.R
import kotlinx.android.synthetic.main.progress_loading.*

class MyProgressBar {
    private val frameAnimation: AnimationDrawable? = null
    private var dialog: Dialog? = null
    fun showProgressDialog(
        context: Context?,
        title: String?,
        msg: String?
    ) {
        try {
            dialog = Dialog(
                context!!,
                android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar
            )
            dialog!!.window
                ?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog!!.setContentView(R.layout.progress_loading)
            dialog!!.setCancelable(false)
            dialog!!.setTitle(title)
            dialog!!.wp7progressBar.showProgressBar()
            dialog!!.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun dismissProgressDialog() {
        try {
            if (dialog != null) {
                if (dialog!!.isShowing) {
                    dialog!!.dismiss()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        //	private SimpleArcDialog mDialog;
        var instance: MyProgressBar? = null
            get() {
                if (field == null) {
                    field = MyProgressBar()
                }
                return field
            }
            private set

    }
}