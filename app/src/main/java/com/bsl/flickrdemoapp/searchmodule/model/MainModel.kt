package com.bsl.flickrdemoapp.searchmodule.model


import com.bsl.flickrdemoapp.searchmodule.api.*
import com.gmbmerchant.retrofit.RetrofitClient
import com.google.gson.JsonObject
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.Retrofit

class MainModel {
    private var mRetrofit: Retrofit? = null

    fun SearchAttribute(text: String, page:Int): Single<PhotosResponse> {
        return RetrofitClient?.apiInterface?.getPhotosMetaData(page,NETWORK_PAGE_SIZE,
            PHOTO_META_DATA_METHOD, API_KEY, USER_ID, FORMAT, NO_JSON_CALLBACK, text)
    }
}