package com.gmbmerchant.promo.model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bsl.flickrdemoapp.searchmodule.api.SchedulersWrapper
import com.bsl.flickrdemoapp.searchmodule.view.ISearchView

class SearchViewModelFactory(val schedulersWrapper: SchedulersWrapper, val searchView : ISearchView) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(SearchViewModel::class.java)) {
            SearchViewModel(schedulersWrapper, searchView) as T
        } else {
            throw IllegalArgumentException()
        }
    }
}